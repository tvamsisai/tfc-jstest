# Test Coverage

## Server
1.	Haskell is type-safe, and the computations in the applications are done in pure functions and environment.

2. 	Although there is one case that could hamper the performance of the server application, which is the use of MVar. As seen in the benchmarking of `/api/global-counter/` endpoint, we do not find any deviation from normal working.

3.	Testing of the connection performance deviation with larger input values was performed which showed +/-0.275ms deviation from the mean was found on trials.

4.	During the tests the machine was using a 64-bit environment which restricts the values of numbers above 0 and below 9223372036854775806 (2^63-2) for the input of `/api/counter` endpoint. Also, output cannot exceed 9223372036854775807 (2^63-1) for either `/api/counter` or `/api/global-counter`.


## Client
1.	Input given using the client to the server test positive.

2.	The client running bounded integer Javascript implementation of ES7 can only represent values till 9007199254740992 (2^53) of integers. The client breaks the application after that number.

3. 	Represents the application in responsive manner to the screen size and the device of the client.

4. 	Ran tests to send at most 1000 random requests asyncronously to the server and to represent it on the client, which showed positive results.

5. 	The client loads the application on the latest Google Chrome browser in less than a second in local evironment, with a cached copy of the resources. The static resources uncached are loaded at a maximum of 1.2s on the same test environment.