{-# LANGUAGE OverloadedStrings #-}
module CounterError where

import           Data.Aeson

data CounterError = CounterError {
    inp :: Int,
    out :: String
} deriving (Show)

instance ToJSON CounterError where
    toJSON (CounterError inpr outr) = object ["input" .= inpr, "output" .= outr]
