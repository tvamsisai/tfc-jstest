{-# LANGUAGE OverloadedStrings #-}
module Main where

import           Control.Applicative
import           Snap.Core
import           Snap.Util.FileServe
import           Snap.Http.Server
import           Counter (counterHandler)
import           Hash (hashHandler)
import           GlobalCounter (globalCounterHandler)
import           Control.Concurrent.MVar

main :: IO ()
main = do
    lastCountMVar <- newMVar (0 :: Int)
    quickHttpServe (site lastCountMVar)

site :: MVar Int -> Snap ()
site lastCount =
    ifTop (writeBS "Go to /jstest") <|>
    route [ ("api/counter/:number", counterHandler)
          , ("api/hash/:value", hashHandler)
          , ("api/global-counter/:number", (globalCounterHandler lastCount))
          , ("api/counter", counterHandler)
          , ("api/hash", hashHandler)
          , ("api/global-counter", (globalCounterHandler lastCount))
          ] <|>
    dir "jstest" (serveDirectory "../client/")