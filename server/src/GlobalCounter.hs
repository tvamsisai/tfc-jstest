{-# LANGUAGE OverloadedStrings #-}
module GlobalCounter where

import           Snap.Core
import           Snap.Extras.JSON
import           Data.Aeson
import           Data.ByteString.Char8 (unpack)
import           Text.Read (readMaybe)
import           CounterError
import           Control.Concurrent.MVar
import           Control.Monad.IO.Class

data GlobalCounter = GlobalCounter {
    inpu :: Int,
    oldc :: Int,
    outc :: Int
} deriving (Show)

instance ToJSON GlobalCounter where
    toJSON (GlobalCounter inpr old outpr) = object ["input" .= inpr, "old" .= old, "output" .= outpr]

createGlobalCounter :: Int -> Int -> GlobalCounter
createGlobalCounter m n = GlobalCounter n m 0

computeGlobalCounter :: GlobalCounter -> GlobalCounter
computeGlobalCounter (GlobalCounter inpcc oldcc _) = GlobalCounter inpcc oldcc (inpcc + oldcc)

getOutput :: GlobalCounter -> Int
getOutput (GlobalCounter _ _ outr) = outr

storeCount :: MVar Int -> GlobalCounter -> Snap ()
storeCount lastMVar n = do
    liftIO $ putMVar lastMVar (getOutput n)
    writeJSON n

checkValid :: MVar Int -> Int -> Snap ()
checkValid m n
    | n < 0 = do
        writeJSON (CounterError n "Invalid Input")
    | otherwise = do
        l <- liftIO $ takeMVar m
        ((storeCount m) . computeGlobalCounter . (createGlobalCounter l)) n

globalCounterHandler :: MVar Int -> Snap ()
globalCounterHandler mvar = do
    number <- getParam "number"
    case number of
        Just n -> maybe (writeJSON (CounterError 0 "Not an integer"))
                        (checkValid mvar) ((readMaybe . unpack) n)
        Nothing -> writeJSON (CounterError 0 "Enter integer")