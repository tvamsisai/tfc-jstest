{-# LANGUAGE OverloadedStrings #-}
module Hash where

import           Snap.Core
import           Snap.Extras.JSON
import           Data.Aeson
import           Data.ByteString.Char8 (unpack)
import qualified Data.ByteString.Lazy.Char8 as LBS
import           Data.Digest.Pure.MD5 (md5)

data Hash  = Hash {
    inp :: String,
    out :: String
} deriving (Show)

instance ToJSON Hash where
    toJSON (Hash inpr outpr) = object ["input" .= inpr, "output" .= outpr]

createHash :: String -> Hash
createHash n = Hash n ""

computeHash :: Hash -> Hash
computeHash (Hash inpch _) = Hash inpch ((show . md5 . LBS.pack) inpch)

hashHandler :: Snap ()
hashHandler = do
    value <- getParam "value"
    maybe (writeJSON (Hash "" "Enter string"))
          (writeJSON . computeHash . createHash . unpack) value