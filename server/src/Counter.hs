{-# LANGUAGE OverloadedStrings #-}
module Counter where

import           Snap.Core
import           Snap.Extras.JSON
import           Data.Aeson
import           Data.ByteString.Char8 (unpack)
import           Text.Read (readMaybe)
import           CounterError

data Counter = Counter {
    inp :: Int,
    out :: Int
} deriving (Show)

instance ToJSON Counter where
    toJSON (Counter inpr outpr) = object ["input" .= inpr, "output" .= outpr]

createCounter :: Int -> Counter
createCounter n = Counter n 0

computeCounter :: Counter -> Counter
computeCounter (Counter inpcc _) = Counter inpcc (inpcc + 1)

checkValid :: Int -> Snap ()
checkValid n
    | n < 0 = do
        writeJSON (CounterError n "Invalid Input")
    | otherwise = do
        (writeJSON . computeCounter . createCounter) n

counterHandler :: Snap ()
counterHandler = do
    number <- getParam "number"
    case number of
        Just n -> maybe (writeJSON (CounterError 0 "Not an integer"))
                        (checkValid) ((readMaybe . unpack) n)
        Nothing -> writeJSON (CounterError 0 "Enter integer")