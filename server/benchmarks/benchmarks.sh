#!/bin/bash

# Using ApacheBench to get benchmarking results. This is a special patched 
# version that I found on [1]. It has the -R option that allows appending of
# variables at the end of the URL, which I used to check the throughput at the
# API endpoints.
#
# ./ab runs only on 64-bit Linux based operating systems
# 
# I had first tried using Siege Benchmarking tool, which wasn't able to parse
# parsedown the Snap responses properly in benchmark mode. Hence, got back to
# using ApacheBench. Also, I had considered using wrk tool, which like ab does
# not have the appending functionality.
#
# Usage:
#     ./benchmarks.sh
#			Has 5 concurrent workers which spawn 50,000 requests.
#     ./benchmarks.sh <arguments to ab>
#			Can be used to provide other arguments to ab.
#
# [1]: https://github.com/philipgloyne/apachebench-for-multi-url

echo $@
if [ $# -eq 0 ]; then
	args="-c 5 -n 50000"
else
	args=$@
fi

echo ""
echo "***********************************************************************"
echo "                        Benchmarking /jstest/"
echo "***********************************************************************"
./ab $args http://localhost:8000/jstest/

echo ""
echo "***********************************************************************"
echo "                       Benchmarking /api/hash/"
echo "***********************************************************************"
for (( i = 0; i < 100; i++ )); do
	s=`cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1`
	echo "$s" >> url.txt
done
./ab $args -R url.txt http://localhost:8000/api/hash/
rm url.txt

echo ""
echo "***********************************************************************"
echo "                      Benchmarking /api/counter/"
echo "***********************************************************************"
for (( i = 0; i < 100; i++ )); do
	s=`cat /dev/urandom | tr -dc '0-9' | fold -w 10 | head -n 1`
	echo "$s" >> url.txt
done
./ab $args -R url.txt http://localhost:8000/api/counter/
rm url.txt

echo ""
echo "***********************************************************************"
echo "                   Benchmarking /api/global-counter/"
echo "***********************************************************************"
for (( i = 0; i < 100; i++ )); do
	s=`cat /dev/urandom | tr -dc '0-9' | fold -w 5 | head -n 1`
	echo "$s" >> url.txt
done
./ab $args -R url.txt http://localhost:8000/api/global-counter/
rm url.txt