FROM haskell:7.10.3

RUN apt-get update && apt-get install -y libpcre3-dev pkg-config

WORKDIR /opt/tfc-jstest/server

RUN cabal update

COPY server/server.cabal /opt/tfc-jstest/server/server.cabal
RUN cabal install --only-dependencies -j2


COPY . /opt/tfc-jstest/
RUN cabal install

CMD ["./runserver.sh"]
