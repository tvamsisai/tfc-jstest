var APIDIR = "/api/";
var reqNum = 0;

$(document).ready( function () {
	requestOutput = function(e) {
		obj = {};
		obj.reqNum = ++reqNum;
		obj.input  = $("input[type='text']").val();
		obj.action = $("select option:selected").attr("value");
		obj.value  = $("select option:selected").html();
		obj.reqStr = APIDIR + obj.action + "/" + obj.input;

		$.ajax({
			dataType: "json",
			context: obj,
			url: obj.reqStr,
			success: function (data) {
				s  = "<div class='req'>";
				s += this.value + " (#" + this.reqNum + "): ";
				if(this.action == 'global-counter') {
					s += "<table><tr><th>Input</th><th>Old Counter</th><th>Output</th></tr></tr>";
					s += "<td class='g3'>" + data.input + "</td>";
					s += "<td class='g3'>" + data.old + "</td>";
					s += "<td class='g3'>" + data.output + "</td>";
				} else {
					s += "<table><tr><th>Input</th><th>Output</th></tr></tr>";
					s += "<td>" + data.input + "</td>";
					s += "<td>" + data.output + "</td>";
				}
				s += "</tr></table></div>";
				$(".show").prepend(s);
			}
		});
		e.preventDefault();
		return false;
	};

	$("input[type='submit']").click(requestOutput);
});
